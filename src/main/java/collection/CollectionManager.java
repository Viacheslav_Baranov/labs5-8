package collection;
import collection.data.MusicBand;
import lombok.Getter;

import java.util.*;
import java.util.stream.Stream;

@Getter
public class CollectionManager {
    private PriorityQueue<MusicBand> queue;
    private Date date; /// Change do smth else

    {
        date = new Date();
        queue = new PriorityQueue<>();
    }

    public void saveCollection(){
    }

    public void loadCollection(){
    }

    public int getSize(){
        return queue.size();
    }

    //public void show() {}

    public boolean add(MusicBand band) {
        return queue.add(band);
    }

    public boolean update(int id, MusicBand band) {
        if (!queue.remove(new MusicBand(id))){
            return false;
        };
        band.setId(id);
        queue.add(band);
        return true;
    }

    public boolean remove_by_id(int id) {
        return queue.remove(new MusicBand(id));
    }

    public void clear() {
        queue.clear();
    }

    public MusicBand remove_head() {
        return queue.poll();
    }

    public boolean add_if_max(MusicBand band) {
        if(queue.stream().noneMatch(b -> b.moreThan(band))){
            return queue.add(band);
        }
        return false;
    }

    public boolean remove_lower(MusicBand band) {
        return queue.removeIf(b -> b.lessThan(band));
    }

    public boolean remove_all_by_number_of_participants
            (long numberOfParticipants) {
        return queue.removeIf(band ->
                band.getNumberOfParticipants() < numberOfParticipants);
    }

    public Stream<MusicBand> filter_contains_name(String name) {
        return queue.stream().
                filter(band -> band.getName().contains(name));
    }

    public Stream<MusicBand> descending_order() {
        return queue.stream().sorted();
    }

}
