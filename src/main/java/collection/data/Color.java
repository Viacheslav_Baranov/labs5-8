package collection.data;

import annotations.UserAccess;

public enum Color {
    @UserAccess(description = "Color")
    GREEN,
    RED,
    BLACK;
}