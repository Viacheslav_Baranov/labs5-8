package collection.data;

import annotations.GreaterThan;
import annotations.UserAccess;
import lombok.*;

import java.util.*;

@Getter
@Setter
@ToString(exclude = {})
@NoArgsConstructor
@AllArgsConstructor
//@Slf4j
public class MusicBand implements Comparable<MusicBand> {
    @NonNull
    @UserAccess(description = "id")
    private Integer id; //Поле не может быть null, Значение поля должно быть больше 0, Значение этого поля должно быть уникальным, Значение этого поля должно генерироваться автоматически
    @NonNull
    @UserAccess(description = "band name")
    private String name; //Поле не может быть null, Строка не может быть пустой

    @NonNull
    @UserAccess(description = "location")
    private Coordinates coordinates; //Поле не может быть null

    @NonNull
    //@UserAccess(description = "Date of creation")
    private Date creationDate; //Поле не может быть null, Значение этого поля должно генерироваться автоматически

    @NonNull
    @UserAccess(description = "number of participants")
    private Long numberOfParticipants; //Поле может быть null, Значение поля должно быть больше 0

    @GreaterThan
    @UserAccess(description = "number of albums")
    private int albumsCount; //Значение поля должно быть больше 0
    @NonNull
    @UserAccess(description = "music genre")
    private MusicGenre genre; //Поле может быть null
    @NonNull
    @UserAccess(description = "Head of band")
    private Person frontMan; //Поле может быть null

    {
        id = Math.abs(new Random().nextInt());
        creationDate = new Date();
    }

    public MusicBand(Integer id) {
        this.id = id;
    }


    @Override
    public int compareTo(MusicBand other) {
        return id - other.id;
    }

    public boolean lessThan(MusicBand other) {
        return compareTo(other) < 0;
    }

    public boolean moreThan(MusicBand other) {
        return compareTo(other) > 0;
    }


    public boolean equals(MusicBand other) {
        return Objects.equals(id, other.id);
    }

}
