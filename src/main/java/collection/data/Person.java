package collection.data;

import annotations.UserAccess;
import collection.data.Color;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.ZonedDateTime;

@Setter
@Getter
@ToString(exclude = {})
@AllArgsConstructor
public class Person{
    @UserAccess(description = "person name")
    private String name; //Поле не может быть null, Строка не может быть пустой
    @UserAccess(description = "person birthday")
    private ZonedDateTime birthday; //Поле может быть null
    @UserAccess(description = "passport ID")
    private String passportID; //Длина строки должна быть не меньше 4, Длина строки не должна быть больше 42, Поле не может быть null
    @UserAccess(description = "eye color")
    private Color eyeColor; //Поле не может быть null

    public Person(String name){
        this.name = name;
    }
}
