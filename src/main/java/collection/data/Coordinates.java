package collection.data;

import annotations.NotNull;
import annotations.UserAccess;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString(exclude = {})
public class Coordinates{
    @NonNull
    @UserAccess(description = "Lat")
    private long x; //Поле не может быть null
    @UserAccess(description = "Long")
    private int y;

    public Coordinates(long X, int Y){
        x = X;
        y = Y;
    }
    /*public String toString(){
        return String.valueOf(x) + " " + String.valueOf(y);
    }*/
}
