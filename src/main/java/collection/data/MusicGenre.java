package collection.data;

import annotations.UserAccess;

public enum MusicGenre {
    @UserAccess(description = "Genre")
    PROGRESSIVE_ROCK,
    RAP,
    PSYCHEDELIC_CLOUD_RAP,
    POP;
}